### Problem

(What’s the problem that this pattern solves? Why is it worth solving?)

### Solution

(What’s the solution? Why is it like that? What are the benefits?)

### Example(s)

(One or more images showing the UX pattern. They don’t have to be in GitLab.)

### Usage

(When do you use this pattern? And how?)

#### Dos and dont's

(Use this table to add images and text describing what’s ok and not ok.)

| :white_check_mark:  Do | :stop_sign: Don’t |
|------------------------|-------------------|
|  |  |

### Related patterns

(List any related or similar solutions. If none, write: No related patterns)

### Links / references

### Pattern checklist

Make sure these are completed before closing the issue,
with a link to the relevant commit, if applicable.

1. [ ] Ensure that you have broken things down into atoms, molecules, and organisms.
1. [ ] Check that you have not created a duplicate of an existing pattern.
1. [ ] Ensure that you have used the proper method for creating the pattern depending on the complexity. Atoms and molecules are symbols, organisms are groups.
1. [ ] Make sure that text is created using text styles. When applicable used shared styles for colors.
1. [ ] QA check by another UX'r (create and reference a file in this issue which includes the changes as you would like to add them to the gitlab-elements file)
1. [ ] Added to gitlab-elements.sketch
1. [ ] Add to the [UX Guide](https://docs.gitlab.com/ce/development/ux_guide/) and/or add to the GitLab Design Library
1. [ ] Add an agenda item to the next UX weekly call to inform everyone (if new pattern, not yet used in the application)

/label ~"UX"
/cc @cperessini @dimitrieh @hazelyang @pedroms @sarrahvesselov @sarahod @tauriedavis
